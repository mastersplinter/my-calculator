job ('python job')
{
    scm{
        git('https://gitlab.com/mastersplinter/my-calculator.git'){ node ->
            node / gitConfigName('DSL script')
            node / gitConfigEmail('jenkins@fakeemail.com')

        }
    }

    triggers {
        scm('H/5 * * * *')

    }


    steps{
        shell('python -m unittest tester.py')
        

    }

}
